#!/usr/bin/gnuplot
# gnuplot -c plot.gnu ARG1 ARG2 ARG3"
print "gnuplot -c plot.gnu out_eps channel1 channel2"
print "gnuplot -c ". ARG0." ".ARG1." ".ARG2." ".ARG3

file_out=ARG1

set key left top
#Lines constants
set parametric
px1=-4.7
px2=px1+1
py1=-4
py2=-0.6
set trange [-40:0]

#end constants
set terminal eps
set output file_out
set datafile separator ","
set grid
set xlabel "P_{in}[dBm]" #"labelx"
set ylabel "P_{out}[dBm]" #"labely"
set title "P_{in} vs P_{out}" #"titleGraph"
m=1
n=2
plot ARG2 using m:n with line title "" linecolor rgb "red", py1,t notitle, py2,t notitle,  t,px1 notitle, t,px2 notitle, py1,px1 w p ls 3 title "-4.7, -4", py2,px2 w p ls 2 title "-3.7, -0.6" #with lp # m <- column, n <- this column, (1*$n):($m/1)
#plot ARG2 using m:n with line title "" linecolor rgb "red", ARG3 using m:n with line title "" linecolor rgb "blue" #with lp # $m <- column, $n <- this column, (1*$n):($m/1)

