#!/usr/bin/gnuplot
# gnuplot -c plot.gnu ARG1 ARG2 ARG3"
print "gnuplot -c plot.gnu out_eps channel1 channel2"
print "gnuplot -c ". ARG0." ".ARG1." ".ARG2." ".ARG3

file_out=ARG1

set key left top
#Lines constants
set parametric
px1=-26.5
px2=-24
py1=-4.9
py2=py1+1
set trange [-20:-0]
#set xrange [-40:-20]
#end constants
set terminal eps
set output file_out
set datafile separator ","
set grid
set xlabel "P_{in}[dBm]" #"labelx"
set ylabel "P_{out}[dBm]" #"labely"
set title "P_{in} vs P_{out}" #"titleGraph"
m=1
n=3
plot ARG2 using m:n with line title "" linecolor rgb "red",\
    px1,t notitle, \
    px2,t notitle,\
    t-20,py1 notitle,\
    t-20,py2 notitle,\
    px1,py1 w p ls 3 title "-26.5, -4.9",\
    px2,py2 w p ls 2 title "-24, -3.9" #with lp # m <- column, n <- this column, (1*$n):($m/1)
#plot ARG2 using m:n with line title "" linecolor rgb "red", ARG3 using m:n with line title "" linecolor rgb "blue" #with lp # $m <- column, $n <- this column, (1*$n):($m/1)

