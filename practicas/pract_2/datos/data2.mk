script = data2.gnu
csv1 = datos2.csv
csv2 =
#out = $(script)
out =datos2

create_eps: $(script)
	gnuplot -c $(script) $(out).eps $(csv1) $(csv2)

all: create_eps
	evince $(out).eps

edit:
	vim -o Makefile $(script)

clear:
	rm $(out).eps

help:
	@echo "make all"
	@echo "make out=csv1"
	@echo "make script=file.gnu"
	@echo "make csv1=file.csv"
