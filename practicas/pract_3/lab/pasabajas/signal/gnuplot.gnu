#!/usr/bin/gnuplot
# gnuplot -c plot.gnu ARG1 ARG2 ARG3"
print "gnuplot -c plot.gnu out_eps channel1 channel2"
print "gnuplot -c ". ARG0." ".ARG1." ".ARG2." ".ARG3

file_out=ARG1

px1=11
py1=-0.05

#set style line 1 lc rgb 'blue' pt 7

set terminal eps
set output file_out
set datafile separator ","
set grid
set xlabel "Tiempo [nS]" #"labelx"
set ylabel "Voltaje [V]" #"labely"
set title "Vista de la repuesta del filtro pasabajas en el osciloscopio" #"titleGraph"
m=1
n=3
plot ARG2 using (1E9*$1):2 with line title "Repuesta después de pasar por filtro pasabajas" linecolor rgb "red"

#with lp # m <- column, n <- this column, (1*$n):($m/1)
#plot ARG3 using m:n with line title "" linecolor rgb "red", ARG3 using m:n with line title "" linecolor rgb "blue" #with lp # $m <- column, $n <- this column, (1*$n):($m/1)

