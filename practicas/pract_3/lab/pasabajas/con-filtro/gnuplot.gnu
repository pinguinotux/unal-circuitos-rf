#!/usr/bin/gnuplot
# gnuplot -c plot.gnu ARG1 ARG2 ARG3"
print "gnuplot -c plot.gnu out_eps channel1 channel2"
print "gnuplot -c ". ARG0." ".ARG1." ".ARG2." ".ARG3

file_out=ARG1

px1=11
py1=-0.04

set style line 1 lc rgb 'blue' pt 7

set terminal eps
set output file_out
set datafile separator ","
set grid
set xlabel "Frec [Mhz]" #"labelx"
set ylabel "Pot [dBm]" #"labely"
set title "Los 5 primeros armónicos de una señal cuadrada a 11 Mhz con filtro pasabandas" #"titleGraph"
m=1
n=3
plot ARG2 using (1E-6*$1):3 with line title "" linecolor rgb "red",\
  "<echo '11, -0.04'" with points ls 1 title "11 Mhz, -0.04 dBm"
#with lp # m <- column, n <- this column, (1*$n):($m/1)
#plot ARG3 using m:n with line title "" linecolor rgb "red", ARG3 using m:n with line title "" linecolor rgb "blue" #with lp # $m <- column, $n <- this column, (1*$n):($m/1)

