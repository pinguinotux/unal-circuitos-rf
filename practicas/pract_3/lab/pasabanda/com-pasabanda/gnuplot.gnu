#!/usr/bin/gnuplot
# gnuplot -c plot.gnu ARG1 ARG2 ARG3"
print "gnuplot -c plot.gnu out_eps channel1 channel2"
print "gnuplot -c ". ARG0." ".ARG1." ".ARG2." ".ARG3

file_out=ARG1

px1=11
py1=-0.04

set style line 1 lc rgb 'blue' pt 7

set terminal eps
set output file_out
set datafile separator ","
set grid
set xlabel "Frec [Mhz]" #"labelx"
set ylabel "Pot [dBm]" #"labely"
set title " Observación de la acción del filtro pasabanda" #"titleGraph"
m=1E-6
n=3
#plot ARG2 using (1E-6*$1):3 with line title "" linecolor rgb "red",\
#  "<echo '104.3, -56.12'" with points ls 1 title "104.3 Mhz, -56.12 dBm"
plot ARG2 using (m*$1):($3) with line title "Con filtro pasabanda" linecolor rgb "red", ARG3 using (m*$1):($3) with line title "Sin filtro pasabanda" linecolor rgb "blue" #with lp # $m <- column, $n <- this column, (1*$n):($m/1)
#with lp # m <- column, n <- this column, (1*$n):($m/1)
#plot ARG3 using m:n with line title "" linecolor rgb "red", ARG3 using m:n with line title "" linecolor rgb "blue" #with lp # $m <- column, $n <- this column, (1*$n):($m/1)

