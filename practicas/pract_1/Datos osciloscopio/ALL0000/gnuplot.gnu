#!/usr/bin/gnuplot
# gnuplot -c plot.gnu ARG1 ARG2 ARG3"
print "gnuplot -c plot.gnu out_eps file1 file2"
print "gnuplot -c ". ARG0." ".ARG1." ".ARG2." ".ARG3

file_out=ARG1

set style line 1 lc rgb 'black' pt 5   # square
set style line 2 lc rgb 'red' pt 7   # circle
set style line 3 lc rgb 'green' pt 9   # triangle

set terminal eps
set output file_out
set datafile separator "," #" " #"," verificar separador del archivo inicialmente
set grid
set xlabel "T[nS]" #"labelx"
set ylabel "Volt[mV]" #"labely"
#set y2label "Fase[°]" #"labely"
#set y2tics
set title "Gráfica del parámetro S11" #"titleGraph"
m=1E3
n=1E10
#plot "< join ".ARG2." ".ARG3 using 5:10 every ::21  with line title "dB(S11)" linecolor rgb "red"#with lp # m <- column, n <- this column, (1*$n):($m/1)
plot ARG2 using ($5):($10) every ::20 with line title "dB(S11)" linecolor rgb "red"#with lp # m <- column, n <- this column, (1*$n):($m/1)
#plot ARG2 using ($1/n):($2) every ::4 with line title "dB(S11)" linecolor rgb "red"#with lp # m <- column, n <- this column, (1*$n):($m/1)
#plot ARG2 using ($1/n):($2) every ::4 with line title "dB(S11)" linecolor rgb "red"#with lp # m <- column, n <- this column, (1*$n):($m/1)
#plot ARG2 using ($1/n):($2) every ::4 with line title "db(11)" linecolor rgb "red" axes x1y1, ARG2 using ($1/n):($3) every ::4 with line title "fase" linecolor rgb "blue" axes x2y2, '-' w p ls 3 title'1.94 Ghz \@ -38 dB', '-' w p ls 2 title 'BW 170 'BW 170MHz'MHz' #with lp # $m <- column, $n <- this column, (1*$n):($m/1)
#1.94 -38 "hola"
#e
#2.02 -10
#1.85 -10
#e
