cs = 10E-6
l1 = 470E-9
c1b = 10E-12
c1a = 10E-12
c2 = 100E-12
lrfc = 1E-3
r2 = 10E3
r1 = 20E3
ce = 10E-6
re = 47
ree = 500
rc = 500

output = [".param"\
  " cs_val=" mat2str(cs)\
  " l1_val=" mat2str(l1)\
  " c1b_val=" mat2str(c1b)\
  " c1a_val=" mat2str(c1a)\
  " c2_val=" mat2str(c2)\
  " lrfc_val=" mat2str(lrfc)\
  " r2_val=" mat2str(r2)\
  " r1_val=" mat2str(r1)\
  " ce_val=" mat2str(ce)\
  " re_val=" mat2str(re)\
  " ree_val=" mat2str(ree)\
  " rc_val=" mat2str(rc)\
  ]

%output = [
%  ".control\n"\
%  " let var_cs = " mat2str(cs) "\n"\
%  " let var_l1 = " mat2str(l1) "\n"\
%  " let var_c1b = " mat2str(c1b) "\n"\
%  " let var_c1a = " mat2str(c1a) "\n"\
%  " let var_c2 = " mat2str(c2) "\n"\
%  " let var_rfc = " mat2str(lrfc) "\n"\
%  " let var_r2 = " mat2str(r2) "\n"\
%  " let var_r1 = " mat2str(r1) "\n\n"\
%  " alter cs var_cs\n"\
%  " alter l1 var_l1\n"\
%  " alter c1b var_c1b\n"\
%  " alter c1a var_c1a\n"\
%  " alter c2 var_c2\n"\
%  " alter lrfc var_rfc\n"\
%  " alter r2 var_r2\n"\
%  " alter r1 var_r1\n"\
%  ".endc\n"\
%  ]

filename = "oscilator.param"
fid = fopen(filename, "w")
fputs(fid, output)
fclose(fid)

%printf("hello '%d%%' ", cs);

