lrfc = 1E-3 % 1mHenrio
cs = 10E-6  % 10µF Lo más grande posible
c2 = 100E-12 % 12pF
r2 = 10E3 % 10KΩ Resistencias de polarización
r1 = 20E3 % 20Komhs
ce = 10E-6 % µF
re = 47 % 47 Omhs 
ree = 500 % 500 Omhs
rc = 500  %500 Omhs

fo = 90E6; % 96.5MHz
c1a = 10E-12; % 10pF
c1b = 7.5E-12 %c1a

C = (c1a*c1b)/(c1a+c1b)

L = 1/(C*(2*pi*fo)^2)

l1 = 470E-9

output = [".param"\
  " cs_val=" mat2str(cs)\
  " l1_val=" mat2str(l1)\
  " c1b_val=" mat2str(c1b)\
  " c1a_val=" mat2str(c1a)\
  " c2_val=" mat2str(c2)\
  " lrfc_val=" mat2str(lrfc)\
  " r2_val=" mat2str(r2)\
  " r1_val=" mat2str(r1)\
  " ce_val=" mat2str(ce)\
  " re_val=" mat2str(re)\
  " ree_val=" mat2str(ree)\
  " rc_val=" mat2str(rc)\
  ]

filename = "oscilator.param"
fid = fopen(filename, "w")
fputs(fid, output)
fclose(fid)

