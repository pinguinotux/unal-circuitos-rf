* 1N5250     20 Volt �5% �W zener diode
.model D1N5250  D(Is=10.18f Rs=6.578 Ikf=0 N=1 Xti=3 Eg=1.11 Cjo=59p M=.4063
+               Vj=.75 Fc=.5 Isr=1.415n Nr=2 Bv=20 Ibv=21.603m Nbv=1.2514
+               Ibvl=218.21u Nbvl=1.2514 Tbv1=850u)
*               Motorola        pid=1N5250      case=DO-35
*               89-9-18 gjg
*               Vz = 20 @ 6.2mA, Zz = 39 @ 1mA, Zz = 13 @ 5mA, Zz = 8.25 @ 20mA

