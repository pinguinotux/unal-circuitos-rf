#!/bin/bash -e
#
# Brief:	Configurador para el módulo oscilador
# Author: Johnny Cubides
# e-mail: jgcubidesc@unal.edu.co
# date: Saturday 23 February 2019

if [ ! -L QSS9018 ]; then
  ln -s ../../component/SS9018.mod QSS9018
else
  echo `ls -l QSS9018`" simbolic link exist"
fi

