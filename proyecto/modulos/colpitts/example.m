format short eng
% Característica del transistor
ft = 1.1E9 %1.1GHz frecuencia de corte alta
hfe = 100

%*************************
%*************************
% # CIRCUITO COLPITTS
%*************************
%*************************

%*****************************
% ##  Frecuencia de resonancia
%*****************************

%fr = 1/(2*pi*sqrt(L*C)) % Frecuencia de resonancia
% fr es una aproximación muy precisa con un 
% error del 1% siempre que Q > 10

% Capacidad equivalente
%C_eq = (c1+c2)/(c1+c2)

%**************************
% ##  Condición de Arranque
%**************************
%A_v*B > 1
%A_v > 1/B
% Fracción de realimentación
%B = c1/c2
% la ganancia de tensión mínima es
%A_v_min = c2/c1

%**************************
% ###  Valor de Av
%**************************
% Ganancia en tensión
%A_v = r_c/r_e

function [C] = get_C(L,fo)
  %disp("Calculando capacitancia del oscilador COLPITTS");
  C = 1/(L*(2*pi*fo)^2);
  %disp(C);
endfunction

function [c2] = get_c2(C, c1)
  %disp("A partir de: C y c1 se obtiene C2");
  %disp("Que hace parte del tanque oscilador");
  %disp("C:"), disp(C);
  %disp("c1:"), disp(c1);
  %disp("Valor de c2");
  c2 = (c1*C)/(c1-C);
  %disp(c2)
endfunction

% Sugerencia de diseño
% R_B << (Betha+1)R_E

function [R_B] = get_R_B(R_E, Betha)
  %disp("Obteniendo resistencia R de base");
  R_B = 0.1*Betha*R_E;
  %disp("R_B"), disp(R_B);
endfunction

function [R_eq] = get_parallel(r1, r2)
  R_eq = (r1*r2)/(r1+r2);
endfunction

function [Ic] = get_Ic(vcc, rca, rcd)
  %disp("Obteniendo corriente de colector");
  Ic = vcc/(rca+rcd);
endfunction

function [Vbb] = get_Vbb(R_B, R_E, Betha, Ic, V_BE)
  Vbb = ((R_B/Betha)+R_E)*Ic + V_BE;
endfunction

function [R1] = get_R1(R_B, Vbb, Vcc)
  R1 = R_B/(1-(Vbb/Vcc));
endfunction

function [R2] = get_R2(R_B, Vcc, Vbb)
  R2 = R_B*(Vcc/Vbb);
endfunction

function [re] = get_re(Ie)
  % re = Vt/Ie = 1/gm
 re = 25.7E-3 / Ie;
endfunction

function [cap] = get_capacitance(f,z)
  cap = 1/(2*pi*f*z);
endfunction

function r_norma = get_R_norma(Resistor, Serie)
  % http://www.lcardaba.com/articles/R_normal/R_normal.htm
  % Serie       E6, E12, E24, E48, E96, E192
  % Tolerancia  20,  10,   5,   2,   1,  0.5 porcentaje
  numberSerie = round(Serie*log10(Resistor)+1);
  r_norma = 10^((numberSerie -1)/Serie);
endfunction

function c_norma = get_C_norma(Condensador, Serie)
  % http://www.lcardaba.com/articles/R_normal/R_normal.htm
  % Serie       E6, E12, E24, E48, E96, E192
  % Tolerancia  20,  10,   5,   2,   1,  0.5 porcentaje
  condesador_en_pF = Condensador * 1E12
  numberSerie = round(Serie*log10(condesador_en_pF)+1);
  c_norma = 10^((numberSerie -1)/Serie)*1E-12;
endfunction

disp("CONDICIONES INICIALES")

lrfc = 1E-3
c1 = 33E-9 % Asumido
fo = 100E3  %
L = 100E-6
Betha = 196
Vcc = 12
R_E = 500
R1 = 10E3
R_CD = 1E3
V_BE = 0.7
RL =10E3

disp("CALCULANDO")

C = get_C(L,fo)
c2 = get_c2(C, c1)

R_B = get_R_B(R_E, Betha)
R_CA = get_parallel(R_E, R1)

Ic = get_Ic(Vcc, R_CA, R_CD)
Ie = Ic

VR2 = get_Vbb(R_B, R_E, Betha, Ic, V_BE)
Vbb = VR2

R1 = get_R1(R_B, VR2, Vcc)
R2 = get_R2(R_B, Vcc, Vbb)

r_e = get_re(Ie)

ZB = get_parallel(R1, get_parallel(R2, Betha*r_e))
C_B = get_capacitance(70E3, ZB)

ZE = get_parallel(r_e, R_E)
C_E = get_capacitance(70E3, ZE)

ZO = RL
C_O = get_capacitance(70E3, ZO)

disp("ASIGNANDO VALORES")

cb = C_B
l1 = L
c1 = c1
c2 = c2
lrfc = lrfc
r1 = get_R_norma(R1, 24) % E24, E48
r2 = get_R_norma(R2, 24)
ce = C_E
re = get_R_norma(R_E,24)
rl = RL
co = C_O

disp("PARAMETRIZANDO PARA SPICE")

output = [".param"\
  " cb_val=" mat2str(cb,4)\
  " l1_val=" mat2str(l1)\
  " c1_val=" mat2str(c1,4)\
  " c2_val=" mat2str(c2,4)\
  " lrfc_val=" mat2str(lrfc)\
  " r1_val=" mat2str(r1,3)\
  " r2_val=" mat2str(r2,3)\
  " ce_val=" mat2str(ce,4)\
  " re_val=" mat2str(re,3)\
  " rl_val=" mat2str(rl,3)\
  " co_val=" mat2str(co,4)\
  ]

filename = "oscilator.param"
fid = fopen(filename, "w");
fputs(fid, output)
fclose(fid);

